﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    [SerializeField]
    GameObject[] Spawners = new GameObject[18];

    [SerializeField]
    GameObject StrongEnemy;

    [SerializeField]
    GameObject WeakEnemy;

    private float cd;
    private float minCd = 0.25f;
    private float maxCd = 10;
    private float now = 0;
    private GameObject currentSpawn;

	// Use this for initialization
	void Start ()
    {
        cd = maxCd;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(Time.time >= now)
        {
            now = Time.time + cd;

            currentSpawn = Spawners[Random.Range(0, 18)]; //pick a random spawner from the array

            if(Random.Range(0, 2) == 0)
            {
                Instantiate(WeakEnemy, currentSpawn.transform.position, currentSpawn.transform.rotation);
            }
            else
            {
                Instantiate(StrongEnemy, currentSpawn.transform.position, currentSpawn.transform.rotation);
            }

            if(cd > minCd) //min cd to help prevent game crashing
            {
                cd -= (cd / maxCd); //enemies spawn faster and faster
            }
        }
	}
}
