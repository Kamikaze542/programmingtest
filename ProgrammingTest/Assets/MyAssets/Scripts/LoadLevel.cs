﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour {

    [SerializeField]
    string level;

	public void LevelLoader()
    {
        if(Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene(level);
    }
}
