﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    [SerializeField]
    Text health = null;
    [SerializeField]
    Text ammo = null;
    [SerializeField]
    Text lives = null;
    [SerializeField]
    Text score = null;
    [SerializeField]
    Text highScore = null;
    [SerializeField]
    Text reload = null;
    //script singleton
    private static Score instance = null;
	// Use this for initialization
	void Awake ()
    {
	    if(instance == null)
        {
            GameObject.DontDestroyOnLoad(this.gameObject);
            instance = this;
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
        reload.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.health.text = "Health: " + PlayerData.Instance.Health + "/2";
        this.ammo.text = "Ammo: " + PlayerData.Instance.Ammo + "/12";
        this.lives.text = "Lives: " + PlayerData.Instance.Lives;
        this.score.text = "Score: " + PlayerData.Instance.Score;
        this.highScore.text = "High Score: " + PlayerData.Instance.HighScore;

        //you can reload at any time, but the message doesn't appear unless you are out of ammo
        if(PlayerData.Instance.Ammo == 0)
        {
            reload.enabled = true;
        }
        else
        {
            reload.enabled = false;
        }
	}
    void OnLevelWasLoaded()
    {
        if(SceneManager.GetActiveScene().name == "GameOver")
        {
            this.health.enabled = false;
            this.ammo.enabled = false;
            this.reload.enabled = false;
            this.lives.enabled = false;
            this.score.enabled = true;
            this.highScore.enabled = true;
        }
        else if(SceneManager.GetActiveScene().name == "Menu")
        {
            this.health.enabled = false;
            this.ammo.enabled = false;
            this.reload.enabled = false;
            this.lives.enabled = false;
            this.score.enabled = false;
            this.highScore.enabled = true;
            PlayerData.Instance.Score = 0;
            PlayerData.Instance.Ammo = 12;
            PlayerData.Instance.Health = 2;
            PlayerData.Instance.Lives = 3;
        }
        else if (SceneManager.GetActiveScene().name == "Main")
        {
            this.health.enabled = true;
            this.ammo.enabled = true;
            this.reload.enabled = false;
            this.lives.enabled = true;
            this.score.enabled = true;
            this.highScore.enabled = true;
            PlayerData.Instance.Score = 0;
            PlayerData.Instance.Ammo = 12;
            PlayerData.Instance.Health = 2;
            PlayerData.Instance.Lives = 3;
        }
    }
}
