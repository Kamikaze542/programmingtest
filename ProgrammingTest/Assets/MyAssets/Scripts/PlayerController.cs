﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    private Rigidbody rb;
    private Animator anim;
    private Ray ray;
    private RaycastHit hit;
    public int maxHp = 2;
    public int hp;
    private int ammoCount;
    private int maxAmmo = 12;
    public bool dying = false;
    public bool respawnInv = false; //when true player cannot be damaged, true for 3 seconds after player is spawned (or respawned)
    private float spd = 10f;
    private float projectileSpd = 20f;
    private float now = 0f; //stores time bullet was fired for weapon cd
    private float cd = 0.25f; // weapon cooldown
    private bool reloading = false;
    public GameObject projectile;
	// Use this for initialization
	void Start ()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
        anim = this.gameObject.GetComponent<Animator>();
	}

    void Awake()
    {
        hp = maxHp;
        PlayerData.Instance.Health = hp; //slightly redundant but I added the variables in player data after the fact so it was easier this way than changing everything
        ammoCount = maxAmmo;
        PlayerData.Instance.Ammo = ammoCount; 
        respawnInv = true;
        dying = false;
        StartCoroutine(RespawnInvEnds());
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(hp > 0)
        {
            Move();
            Aim();
            Fire();
        }
	}

    //Controls player movement
    void Move()
    {
        rb.velocity = Vector3.zero;
        anim.SetBool("IsMoving", false);
        if (Input.GetKey(KeyCode.W))
        {
            anim.SetBool("IsMoving", true);
            rb.velocity += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.S))
        {
            anim.SetBool("IsMoving", true);
            rb.velocity -= Vector3.forward;
        }
        if (Input.GetKey(KeyCode.A))
        {
            anim.SetBool("IsMoving", true);
            rb.velocity -= Vector3.right.normalized;
        }
        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("IsMoving", true);
            rb.velocity += Vector3.right.normalized;
        }
        rb.velocity = rb.velocity.normalized; //ensures player does not move faster on diagonals
        rb.velocity *= spd;
    }
    //aims the player towards the mouse
    void Aim()
    {
        if(Time.timeScale != 0)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);
            //rotates the player to look at the mouse (hit point from the raycast) by taking the x and z distances from the player to the hit point and calculating the angle between them
            this.gameObject.transform.eulerAngles = new Vector3(0f, Mathf.Atan2(hit.point.x - this.gameObject.transform.position.x, hit.point.z - this.gameObject.transform.position.z)
                * (180f / Mathf.PI), 0f);
        }
    }
    void Fire()
    {
        if(Input.GetMouseButton(0) && Time.time >= now && ammoCount > 0 && !reloading)
        {
            now = Time.time + cd;
            ammoCount--;
            PlayerData.Instance.Ammo = ammoCount;
            GameObject bullet = Instantiate(projectile, this.transform.position, this.transform.rotation) as GameObject;
            bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * projectileSpd; //fire bullets in direction player is facing
        }
        if(Input.GetMouseButtonDown(1) && !reloading)
        {
            reloading = true;
            StartCoroutine(Reload());
        }
    }
    public void Dead()
    {
        anim.SetBool("Killed", true);
        PlayerData.Instance.Lives--;
    }
    public void Kill()
    {
        if (PlayerData.Instance.Lives > 0) //Game ends when player dies with 1 life remaining, could also do >= 0 and have the game end when the player dies with 0 lives remaining
        {
            Instantiate(this.gameObject, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);
        }
        else
        {
            SceneManager.LoadScene("GameOver");
        }
    }
    //gives player invulnerability for 3 seconds after respawning
    IEnumerator RespawnInvEnds()
    {
        yield return new WaitForSeconds(3f);
        respawnInv = false;
    }
    //reloads ammo
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(2f);
        ammoCount = maxAmmo;
        PlayerData.Instance.Ammo = ammoCount;
        reloading = false;
    }
}
