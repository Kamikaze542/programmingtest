﻿using UnityEngine;
using System.Collections;

public class PlayerData
{
    //singleton class

    private static PlayerData instance = null;

    public static PlayerData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new PlayerData();
            }
            return instance;
        }
    }

    private PlayerData()
    {
        //set high score from data stored in playerprefs
        highScore = PlayerPrefs.GetInt("PTHighScore");
    }

    //Getters and setters for persistant variables
    private int score = 0;

    public int Score
    {
        get
        {
            return this.score;
        }
        set
        {
            score = value;
            if (score > highScore) //automatically updates highscore if score exceeds it in value
            {
                this.highScore = score;
                PlayerPrefs.SetInt("PTHighScore", highScore);
            }
        }
    }

    private int highScore = 0;

    public int HighScore
    {
        get
        {
            return this.highScore;
        }
    }

    private int lives = 3;
    public int Lives
    {
        get
        {
            return this.lives;
        }
        set
        {
            lives = value;
        }
    }

    private int health = 2;
    public int Health
    {
        get
        {
            return this.health;
        }
        set
        {
            health = value;
        }
    }

    private int ammo = 12;
    public int Ammo
    {
        get
        {
            return this.ammo;
        }
        set
        {
            ammo = value;
        }
    }
}
