﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {


    // Use this for initialization
	void Start ()
    {
        StartCoroutine(Destroy());
	}
	
	// Update is called once per frame
	void Update ()
    {

	}

    void OnTriggerEnter(Collider other)
    {
        //Checks if bullet collides with enemy
        if (other.gameObject.tag == "Enemy")
        {
            other.GetComponent<EnemyController>().hp--; //deals damage
            if(other.GetComponent<EnemyController>().hp <= 0 && !other.GetComponent<EnemyController>().dying) //if enemy hit points reach 0 and the enemy isn't already dying (i.e in death anim)
            {
                other.GetComponent<EnemyController>().dying = true;
                other.GetComponent<EnemyController>().Destroy(); //call destroy function from EnemyController
            }
            Destroy(this.gameObject);
        }
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }
}
