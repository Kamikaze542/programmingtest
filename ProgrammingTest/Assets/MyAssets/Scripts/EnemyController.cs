﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public int hp;
    public int maxHp = 1;
    private float spd;
    public bool dying;
    private Animator anim;
    public GameObject player;
    private Rigidbody rb;
    private int prevLives = 3; //used to track when player lives changes (i.e. when the player dies in order to reaccquire target
	// Use this for initialization
	void Start ()
    {
        anim = this.gameObject.GetComponent<Animator>();
        rb = this.gameObject.GetComponent<Rigidbody>();
        hp = maxHp;
        spd = 5.0f / maxHp;
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if(PlayerData.Instance.Lives != prevLives)
        {
            StartCoroutine(ReAccquirePlayer());
        }
        else
        {
            transform.LookAt(player.transform);
            rb.velocity = Vector3.zero;
            if (!anim.GetBool("InRange"))
            {
                Move();
            }
        }
        prevLives = PlayerData.Instance.Lives; //used to check if the player died in the current frame
	}

    void Move()
    {
        rb.velocity = transform.forward * spd;
    }

    public void Destroy()
    {
        //Enter the death animation (which calls the kill function at the end)
        anim.SetBool("Killed", true);
        //Increase players score based on strong/weak enemy
        PlayerData.Instance.Score += maxHp;
    }
    public void Kill()
    {
        Destroy(this.gameObject);
    }
    IEnumerator ReAccquirePlayer()
    {
        /*
        If the timing for this was exact there wouldn't be any errors during play, but since it happens slightly after the player is destroyed and respawned,
        the errors appear in the period between losing the original player as a target and regaining the newly spawned one as a target.
        However, the console errors shown are irrelevant because the problem gets fixed essentially imediately after the error is logged. Undershooting the time would risk
        not finding the correct instance of the player (i.e. the one that is about to be destroyed) so it is safer to overshoot and just ignore the logged errors.
        */
        yield return new WaitForSeconds(1.1f); 
        player = GameObject.FindGameObjectWithTag("Player");
    }
}
