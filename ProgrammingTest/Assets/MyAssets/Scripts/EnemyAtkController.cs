﻿using UnityEngine;
using System.Collections;

public class EnemyAtkController : MonoBehaviour {

    private float now = 0f;
    private float cd = 1.433f; //cooldown = duration of atk animation

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            this.gameObject.GetComponentInParent<Animator>().SetBool("InRange", true); //atk animation plays while player is colliding
            //only deals damage if player is not invulnerable from respawn and if the atk animation has finished
            if (Time.time >= now && !other.GetComponent<PlayerController>().respawnInv && !this.gameObject.GetComponentInParent<EnemyController>().dying)
            {
                now = Time.time + cd;
                other.GetComponent<PlayerController>().hp -= this.gameObject.GetComponentInParent<EnemyController>().maxHp; //deals dmg to player based on if strong or weak enemy
                PlayerData.Instance.Health = other.GetComponent<PlayerController>().hp;
                if (other.GetComponent<PlayerController>().hp <= 0 && !other.GetComponent<PlayerController>().dying)
                {
                    other.GetComponent<PlayerController>().dying = true;
                    other.GetComponent<PlayerController>().Dead();
                }
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            this.gameObject.GetComponentInParent<Animator>().SetBool("InRange", false);
        }
    }
}
